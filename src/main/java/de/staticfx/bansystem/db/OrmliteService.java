/*
 * Decompiled with CFR <Could not determine version>.
 * 
 * Could not load the following classes:
 *  org.bukkit.configuration.file.FileConfiguration
 */
package de.staticfx.bansystem.db;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import de.staticfx.bansystem.Main;

public class OrmliteService {
    private static OrmliteService instance;
    private ConnectionSource connectionSource;

    private OrmliteService() throws Exception {
        String url = Main.getInstance().getConfig().getString("MySQL_URL");
        String user = Main.getInstance().getConfig().getString("MySQL_USERNAME");
        String password = Main.getInstance().getConfig().getString("MySQL_PASSWORD");



        this.connectionSource = new JdbcConnectionSource(url, user, password, new DataType());

        System.out.println("[BanSystem] Versuche Verbindung zum Datenbank - Service aufzubauen.");
    }

    public static OrmliteService getInstance() {
        return instance;
    }

    public <T, ID> Dao<T, ID> createDao(Class<T> clazz) {
        try {
            TableUtils.createTableIfNotExists(this.connectionSource, clazz);
            return DaoManager.createDao(this.connectionSource, clazz);
        }
        catch (Exception e) {
            System.out.println("Keine Verbindung zur Datenbank möglich");
            throw new RuntimeException(e);
        }
    }

    static {
        try {
            instance = new OrmliteService();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

