/*
 * Decompiled with CFR <Could not determine version>.
 */
package de.staticfx.bansystem.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import de.staticfx.bansystem.objects.Type;

@DatabaseTable(tableName="mutes")
public class Mute {
    @DatabaseField(id=true, canBeNull=false)
    private int BanID;
    @DatabaseField()
    private String uuid;
    @DatabaseField
    private String reason;
    @DatabaseField
    private Long endTime;
    @DatabaseField
    private String banner;
    @DatabaseField
    private String timeStamp;
    @DatabaseField
    private String type;
    @DatabaseField
    private boolean permanent;
    @DatabaseField
    private String unbanned;
    @DatabaseField
    private boolean active;

    public Mute() {
    }

    public Mute(int banID, String uuid, String reason, Long endTime, String banner, String timeStamp, Type type, boolean permanent, String unbanned, boolean active) {
        this.uuid = uuid;
        this.reason = reason;
        this.endTime = endTime;
        this.banner = banner;
        this.timeStamp = timeStamp;
        this.type = type.getType();
        this.permanent = permanent;
        this.unbanned = unbanned;
        this.BanID = banID;
        this.active = active;
    }

    public String getUuid() {
        return this.uuid;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public boolean isPermanent() {
        return this.permanent;
    }

    public void setPermanent(boolean permanent) {
        this.permanent = permanent;
    }

    public String getUnbanned() {
        return this.unbanned;
    }

    public void setUnbanned(String unbanned) {
        this.unbanned = unbanned;
    }

    public String getBanner() {
        return this.banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public int getBanID() {
        return this.BanID;
    }

    public void setBanID(int banID) {
        this.BanID = banID;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean activ) {
        this.active = activ;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

