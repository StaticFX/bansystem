/*
 * Decompiled with CFR <Could not determine version>.
 * 
 * Could not load the following classes:
 *  org.bukkit.Server
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.PluginCommand
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.FileConfigurationOptions
 *  org.bukkit.entity.Player
 *  org.bukkit.event.Listener
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.PluginManager
 *  org.bukkit.plugin.java.JavaPlugin
 */
package de.staticfx.bansystem;

import de.staticfx.bansystem.commands.BanCommandExecutor;
import de.staticfx.bansystem.events.LeaveEvent;
import de.staticfx.bansystem.events.OnChatMessage;
import de.staticfx.bansystem.events.OnJoin;
import de.staticfx.bansystem.filemanager.ConfigFileManager;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main
extends JavaPlugin {
    public static Main instance;
    public ArrayList<Player> team = new ArrayList();

    public void onEnable() {
        System.out.println("[BanSystem] System wird aktiviert.");
        instance = this;
        ConfigFileManager.loadFile();
        loadConfig();
        getCommand("punish").setExecutor(new BanCommandExecutor());
        getServer().getPluginManager().registerEvents(new OnJoin(), this);
        getServer().getPluginManager().registerEvents(new OnChatMessage(), this);
        getServer().getPluginManager().registerEvents(new LeaveEvent(), this);

    }

    public static Main getInstance() {
        return instance;
    }

    void loadConfig() {
        getConfig().addDefault("Prefix", "&4&lBanSystem &8>> ");
        getConfig().addDefault("noPermissions", "&4Du &7hast keine Rechte für diesen &6Command.");

        getConfig().addDefault("Syntax", "&c - &aTEMPLATES&c - &7Erhalte eine Liste aller Templates.");
        getConfig().addDefault("Syntax1", "&c - &aADD [C/T] [P]&c - &7Füge einem Spieler einem Bann hinzu.");
        getConfig().addDefault("Syntax2", "&c - &aREMOVE [BID] [PLAYER]&c - &7Lösche einen Bann eines Spielers.");
        getConfig().addDefault("Syntax3", "&c - &aCREATE [N/R/TIME/T]&c - &7Erstelle ein Template.");
        getConfig().addDefault("Syntax4", "&c - &aDELETE [N]&c - &7Lösche ein Template.");
        getConfig().addDefault("Syntax5", "&c - &aINFO [T]&c - &7Erhalte Informationen über ein Template.");
        getConfig().addDefault("Syntax6", "&c - &aCHECK [P]&c - &7Schaue mit welchen BanIDs mit dem Spieler verbunden sind.");
        getConfig().addDefault("Syntax7", "&c - &aRELOAD &c- &7Lade die Dateien erneut.");
        getConfig().addDefault("Syntax8", "&c - &aIDINFO [BID]&c - &7Erhalte Informationen über eine Ban-ID.");
        getConfig().addDefault("Syntax9", "&c - &aCLEAR [T] [P]&c - &7Lösche den Ban-Verlauf eines Spielers.");
        getConfig().addDefault("Syntax10", "&c - &aNOTIFIY &c- &7Toggled die Teambenachrichtigung         .");



        getConfig().addDefault("noTemplates", "&7Im moment existieren noch keine Templates.");
        getConfig().addDefault("createSyntax", "&7Verwende &6/ban create [NAME/REASON/TIME/TYPE].");
        getConfig().addDefault("infoSyntax", "&7Verwende &6/ban info [TEMPLATE].");
        getConfig().addDefault("templateSyntax", "&7Verwende &6/ban templates.");
        getConfig().addDefault("banSyntax", "&7Verwende &6/ban add [CUSTOM/TEMPLATE] [PLAYER].");
        getConfig().addDefault("customSyntax", "&7Verwende &6/ban add [C] [REASON] [TIME] [TYPE] [PLAYER].");
        getConfig().addDefault("templateSyntax", "&7Verwende &6/ban add [TEMPLATE] [PLAYER].");
        getConfig().addDefault("removeSyntax", "&7Verwende &6/ban remove [BID] [PLAYER].");
        getConfig().addDefault("idinfoSyntax", "&7Verwende &6/ban idinfo [BANID].");
        getConfig().addDefault("clearSyntax", "&7Verwende &6/ban clear [TYPE] [PLAYER].");
        getConfig().addDefault("notifySyntax", "&7Verwende &6/ban notify.");


        getConfig().addDefault("banAutomaticExtended", "&7Der Bann wurde automatisch verlängert, da der Spieler schonmal für diesen Grund gebannt war.");

        getConfig().addDefault("allBanRemoved", "&7Dem gewünschten Spieler wurde die Bann-historie geleert.");
        getConfig().addDefault("noTime", "&7Die Zeiteinheit die du verwenden m\u00f6chtest existiert nicht. Verwende &6s/min/h/d/m/p");
        getConfig().addDefault("noType", "&7Die Banmethode die du verwenden m\u00f6chtest existiert nicht. Verwende &6kick/mute/ban");
        getConfig().addDefault("createdTemplate", "&7Du hast erfolgreich ein Template erstellt. Schaue in der Config nach um es zu bearbeiten.");
        getConfig().addDefault("checkSyntax", "&7Verwende &6/ban check [PLAYER].");
        getConfig().addDefault("deleteTemplate", "&7Du hast erfolgreich das Template gel\u00f6scht.");
        getConfig().addDefault("noTemplate", "&7Das gew\u00fcnschte Template konnte nicht gefunden werden.");
        getConfig().addDefault("noPlayer", "&7Der gew\u00fcnschte Spieler war noch nie auf dem Netzwerk online.");
        getConfig().addDefault("bannedCostume", "&7Der gew\u00fcnschte Spieler wurde erfolgreich gebannt.");
        getConfig().addDefault("banScreen", "§7§l>> §4§lVelony! §7§l<<\n\n§cGrund: §7%reason% \n§cBis: §7%time%\n&cBanID: &7%banid%\n&cTS: &7ts.velony.net\n\n\u00a77\u00a7l>> \u00a74\u00a7lDu wurdest gebannt! \u00a77\u00a7l<<");
        getConfig().addDefault("banNotification", "&7Der Spieler &c%target% &7wurde von &a%banner% &7wegen &c%reason% &7f\u00fcr &a%time% &7gebannt!");
        getConfig().addDefault("muteNotification", "&7Der Spieler &c%target% &7wurde von &a%banner% &7wegen &c%reason% &7f\u00fcr &a%time% &7gemuted! &7BanID: &c%banid%");
        getConfig().addDefault("kickNotification", "&7Der Spieler &c%target% &7wurde von &a%banner% &7wegen &c%reason% &7gekickt!");
        getConfig().addDefault("kick", "&7Der gew\u00fcnschte Spieler wurde erfolgreich gekicked!");
        getConfig().addDefault("mute", "&7Der gew\u00fcnschte Spieler wurde erfolgreich gemuted!");
        getConfig().addDefault("muted", "&7Du bist wegen &c%reason% &7bis zum &c%time% &7gemuted. BanID: &c%banid%");
        getConfig().addDefault("reload", "&7Du hast erfolgreich die Dateien erneut geladen.");
        getConfig().addDefault("noBan", "&7Der gewünschte Spieler hat keine Aktiven Bans von diesem Typ.");
        getConfig().addDefault("banRemoved", "&7Der gew\u00fcnschte Spieler wurde erfolgreich von der Strafe entfernt.");
        getConfig().addDefault("noBansAtAll", "&7Der gew\u00fcnschte Spieler ist nicht gebannt.");
        getConfig().addDefault("alreadyBanned", "&7Der gew\u00fcnschte Spieler ist schon f\u00fcr diesen Typ gebannt.");
        getConfig().addDefault("kicked", "\u00a77\u00a7l>> &4&lDu wurdest gekickt! \u00a77\u00a7l<<\n&cGrund: &7%reason%\n\u00a77\u00a7l>> &4&lDu wurdest gekickt! \u00a77\u00a7l<<");
        getConfig().addDefault("infos", "&7Informationen \u00fcber %target%:");
        getConfig().addDefault("unbannable", "&7Der gew\u00fcnschte Spieler ist nicht bannbar.");
        getConfig().addDefault("noBanID", "&7Die gew\u00fcnschte BanID konnte nicht gefunden werden.");
        getConfig().addDefault("JoinedNotify", "&7Du haste die Team-benachrichtigung efolgreich aktiviert.");
        getConfig().addDefault("LeftNotify", "&7Du haste die Team-benachrichtigung efolgreich deaktiviert.");
        getConfig().addDefault("JoinMessage","&8[&a+&8] &a%player%");
        getConfig().addDefault("LeaveMessage","&8[&c-&8] &c%player%");


        getConfig().addDefault("MySQL_URL", "jdbc:mysql://localhost/bansystem");
        getConfig().addDefault("MySQL_USERNAME", "root");
        getConfig().addDefault("MySQL_PASSWORD", "");
        getConfig().options().copyDefaults(true);
        saveConfig();
        reloadConfig();
    }

    public boolean validTime(String time) {
        if (time.endsWith("s")) {
            return true;
        }
        if (time.endsWith("min")) {
            return true;
        }
        if (time.endsWith("h")) {
            return true;
        }
        if (time.endsWith("d")) {
            return true;
        }
        if (time.endsWith("m")) {
            return true;
        }
        if (!time.endsWith("p")) return false;
        return true;
    }

    public Long timeToMilliSeconds(String time) {
        String edit;
        Long result = 0L;
        if (time.endsWith("s")) {
            edit = time.substring(0, time.length() - 1);
            result = Long.parseLong(edit) * 1000L;
        }
        if (time.endsWith("min")) {
            edit = time.substring(0, time.length() - 3);
            result = Long.parseLong(edit) * 60L * 1000L;
        }
        if (time.endsWith("h")) {
            edit = time.substring(0, time.length() - 1);
            result = Long.parseLong(edit) * 60L * 60L * 1000L;
        }
        if (time.endsWith("d")) {
            edit = time.substring(0, time.length() - 1);
            result = Long.parseLong(edit) * 24L * 60L * 60L * 1000L;
        }
        if (!time.endsWith("m")) return result;
        edit = time.substring(0, time.length() - 1);
        return Long.parseLong(edit) * 31L * 24L * 60L * 60L * 1000L;
    }

    public String getConfigString(String config, Boolean prefix) {
        if (!prefix) return this.getConfig().getString(config).replaceAll("&", "§");

        return this.getConfig().getString("Prefix").replaceAll("&", "§") + this.getConfig().getString(config).replaceAll("&", "\u00a7");
    }

    public void onDisable() {
        System.out.println("System wird deaktiviert.");
    }
}

