/*
 * Decompiled with CFR <Could not determine version>.
 *
 * Could not load the following classes:
 *  org.bukkit.OfflinePlayer
 */
package de.staticfx.bansystem.db;

import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.UUID;

import de.staticfx.bansystem.objects.PlayerID;

public class AdminService {
    private static final AdminService INSTANCE = new AdminService();
    private Dao<PlayerID, UUID> adminDao = OrmliteService.getInstance().createDao(PlayerID.class);

    private AdminService() {
    }

    public static AdminService getInstance() {
        return INSTANCE;
    }

    public void addPlayer(PlayerID playerID) {
        try {
            adminDao.create(playerID);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removePlayer(PlayerID playerID) {
        try {
            adminDao.delete(playerID);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isPlayerUnbannable(UUID uuid) {
        try{
            return adminDao.idExists(uuid);
        }catch (Exception e) {
            return false;
        }
    }

}

