/*
 * Decompiled with CFR <Could not determine version>.
 * 
 * Could not load the following classes:
 *  org.bukkit.OfflinePlayer
 */
package de.staticfx.bansystem.db;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import de.staticfx.bansystem.objects.Ban;
import java.sql.SQLException;
import java.util.List;

import org.bukkit.OfflinePlayer;

public class BanService {
    private static final BanService INSTANCE = new BanService();
    private Dao<Ban, Integer> banDao = OrmliteService.getInstance().createDao(Ban.class);

    private BanService() {
    }

    public static BanService getInstance() {
        return INSTANCE;
    }

    public void addBan(Ban ban) {
        try {
            this.banDao.create(ban);
            return;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeAllBans(OfflinePlayer offlinePlayer) {
        List<Ban> ban = getAllBans(offlinePlayer);
        for(Ban b : ban) {
            try{
                banDao.delete(b);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Ban getBan(int banID) {
        try {
            return this.banDao.queryForId(banID);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean banIDExists(int banID) {
        try {
            return this.banDao.idExists(banID);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean hadBeenBanned(OfflinePlayer offlinePlayer) {
        return !getAllBans(offlinePlayer).isEmpty();
    }

    public void removeBan(int banID) {
        Ban ban = BanService.getInstance().getBan(banID);
        ban.setActive(false);
        try {
            this.banDao.update(ban);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Ban> getAllBans(OfflinePlayer offlinePlayer) {
        try {
            QueryBuilder<Ban, Integer> query = this.banDao.queryBuilder();
            Where queryWhere = query.where();
            queryWhere.eq("uuid", offlinePlayer.getUniqueId());
            PreparedQuery<Ban> mutes = query.prepare();
            return this.banDao.query(mutes);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isPlayerBanned(OfflinePlayer offlinePlayer) {
        return getAllBans(offlinePlayer).stream().anyMatch(Ban::isActive);
    }


    public int getAllBansAmount(OfflinePlayer offlinePlayer) {
        return getAllBans(offlinePlayer).size();
    }

    public int getBannedForReason(OfflinePlayer offlinePlayer, String reason) {
        List<Ban> bans = getAllBans(offlinePlayer);
        int result = 0;

        for(Ban b : bans) {
            if(b.getReason().equalsIgnoreCase(reason));
            result++;
        }
        return result;
    }

}

