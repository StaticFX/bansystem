package de.staticfx.bansystem.commands;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.staticfx.bansystem.Main;
import de.staticfx.bansystem.db.AdminService;
import de.staticfx.bansystem.db.BanService;
import de.staticfx.bansystem.db.MuteService;
import de.staticfx.bansystem.db.NotifyService;
import de.staticfx.bansystem.filemanager.ConfigFileManager;
import de.staticfx.bansystem.objects.*;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class BanCommandExecutor implements org.bukkit.command.CommandExecutor
{
    public boolean onCommand(CommandSender s, Command cmd, String string, String[] args)
    {
        if (!s.hasPermission("banSystem.ban")) {
            s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
            return false;
        }

        if (args.length < 1) {
            s.sendMessage(Main.getInstance().getConfigString("Syntax", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax1", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax2", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax3", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax4", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax5", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax6", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax7", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax8", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax9", true));
            s.sendMessage(Main.getInstance().getConfigString("Syntax10", true));
            return false;
        }

        switch (args[0].toUpperCase()) {
            case "TEMPLATES": {
                if (!s.hasPermission("banSystem.templates")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 1) {
                    s.sendMessage(Main.getInstance().getConfigString("templateSyntax", true));
                    return false;
                }
                if (ConfigFileManager.getConfigs().isEmpty()) {
                    s.sendMessage(Main.getInstance().getConfigString("noTemplates", true));
                    return false;
                }
                s.sendMessage(Main.getInstance().getConfigString("Prefix", false));
                for (String message : ConfigFileManager.getConfigs()) {
                    Config config = ConfigFileManager.getConfig(message);
                    s.sendMessage("§c - §7" + message + " §7| " + config.getType().getType() + " §7| " + config.getLenght());
                }
                return false;
            }
            case "CREATE": {
                if (!s.hasPermission("banSystem.create")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 5) {
                    s.sendMessage(Main.getInstance().getConfigString("createSyntax", true));
                    return false;
                }

                String name = args[1];
                String reason = args[2];
                String time = args[3];

                if (!Main.getInstance().validTime(time)) {
                    s.sendMessage(Main.getInstance().getConfigString("noTime", true));
                    return false;
                }
                Type type;

                if (isBanType(args[4])) {
                    type = new Type(Type.BanType.valueOf(args[4].toUpperCase()));
                } else {
                    s.sendMessage(Main.getInstance().getConfigString("noType", true));
                    return false;
                }
                Long test;
                try {
                    if (time.endsWith("min")) {
                        test = Long.parseLong(time.substring(0, time.length() - 3));
                    } else {
                        test = Long.parseLong(time.substring(0, time.length() - 1));
                    }
                } catch (Exception e) {
                    s.sendMessage(Main.getInstance().getConfigString("createSyntax", true));
                    return false;
                }

                boolean permanent = false;

                if (time.equalsIgnoreCase("p")) {
                    time = "0s";
                    permanent = true;
                }

                Config config = new Config(name, time, reason, type, permanent);

                ConfigFileManager.addConfig(config);
                s.sendMessage(Main.getInstance().getConfigString("createdTemplate", true));
                return true;
            }



            case "DELETE": {
                if (!s.hasPermission("banSystem.delete")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                String template = args[1];
                if (ConfigFileManager.configExists(template)) {
                    ConfigFileManager.removeConfig(template);
                    s.sendMessage(Main.getInstance().getConfigString("deleteTemplate", true));
                    return true;
                }
                s.sendMessage(Main.getInstance().getConfigString("noTemplate", true));
                return false;
            }



            case "INFO": {
                if (!s.hasPermission("banSystem.info")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 2) {
                    s.sendMessage(Main.getInstance().getConfigString("infoSyntax", true));
                    return false;
                }
                String config = args[1];

                if (!ConfigFileManager.configExists(config)) {
                    s.sendMessage(Main.getInstance().getConfigString("noTemplate", true));
                    return false;
                }

                Config result = ConfigFileManager.getConfig(config);

                s.sendMessage(Main.getInstance().getConfigString("Prefix", false));
                s.sendMessage("§7Name: §c" + result.getName());
                s.sendMessage("§7Reason: §c" + result.getReason());
                s.sendMessage("§7Time: §c" + result.getLenght());
                s.sendMessage("§7Type: §c" + result.getType().getType());
                return true;
            }



            case "ADD": {
                if (!s.hasPermission("banSystem.add")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }

                if (args.length < 2) {
                    s.sendMessage(Main.getInstance().getConfigString("banSyntax", true));
                    return false;
                }

                if (args[1].equalsIgnoreCase("c")) {
                    if (args.length != 6) {
                        s.sendMessage(Main.getInstance().getConfigString("customSyntax", true));
                        return false;
                    }
                    String reason = args[2];
                    String time = args[3];

                    String timeStamp = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date());

                    de.dytanic.cloudnet.lib.player.OfflinePlayer target = CloudAPI.getInstance().getOfflinePlayer(args[5]);




                    if (!Main.getInstance().validTime(time)) {
                        s.sendMessage(Main.getInstance().getConfigString("noTime", true));
                        return false;
                    }

                    if (!isBanType(args[4])) {
                        s.sendMessage(Main.getInstance().getConfigString("noType", true));
                        return false;
                    }
                    try {
                        Long test;
                        if (time.endsWith("min")) {
                            test = Long.parseLong(time.substring(0, time.length() - 3));
                        } else
                            test = Long.parseLong(time.substring(0, time.length() - 1));
                    } catch (Exception e) {
                        Long test;
                        s.sendMessage(Main.getInstance().getConfigString("banSyntax", true));
                        return false;
                    }


                    Type type = new Type(Type.BanType.valueOf(args[4].toUpperCase()));

                    if (target == null) {
                        s.sendMessage(Main.getInstance().getConfigString("noPlayer", true));
                        return false;
                    }

                    if(AdminService.getInstance().isPlayerUnbannable(target.getUniqueId())) {
                        s.sendMessage(Main.getInstance().getConfigString("unbannable", true));
                        return false;
                    }


                    long endtime = System.currentTimeMillis() + Main.getInstance().timeToMilliSeconds(time);

                    boolean permanent = false;

                    if (time.endsWith("p")) {
                        permanent = true;
                    }

                    Player kicked;

                    OfflinePlayer targetBukkit = Bukkit.getOfflinePlayer(target.getUniqueId());


                    if (type.getType().equalsIgnoreCase("ban")) {
                        if (!s.hasPermission("banSystem.add.customBan")) {
                            s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                            return false;
                        }
                        if (BanService.getInstance().isPlayerBanned(targetBukkit)) {
                            s.sendMessage(Main.getInstance().getConfigString("alreadyBanned", true));
                            return false;
                        }

                        Random rand = new Random();

                        int random = rand.nextInt(100000);

                        while ((BanService.getInstance().banIDExists(random)) || (MuteService.getInstance().banIDExists(random))) {
                            random = rand.nextInt(100000);
                        }

                        Ban ban = new Ban(random, target.getUniqueId(), reason, endtime, s.getName(), timeStamp, type, permanent, new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date(endtime)), true);

                        BanService.getInstance().addBan(ban);
                        String unbanned = BanService.getInstance().getBan(random).getUnbannedDate();

                        if (BanService.getInstance().getBan(random).isPermanent()) {
                            unbanned = "PERMANENT";
                        }


                        if (CloudAPI.getInstance().getOnlinePlayer(target.getUniqueId()) != null) {
                            CloudAPI.getInstance().getOnlinePlayer(target.getUniqueId()).getPlayerExecutor().kickPlayer(null,Main.getInstance().getConfigString("banScreen", false).replaceAll("%reason%", reason).replaceAll("%time%", unbanned).replaceAll("%banid%", Integer.toString(random)));
                        }


                        for(CloudPlayer cloudPlayer : CloudAPI.getInstance().getOnlinePlayers()) {
                            if(NotifyService.getInstance().isPlayerInNotifycationChat(cloudPlayer.getUniqueId())) {
                                cloudPlayer.getPlayerExecutor().sendMessage(cloudPlayer,"§4Banned! BanID: " + random );
                            }
                        }

                        s.sendMessage(Main.getInstance().getConfigString("bannedCostume", true));
                        return true;
                    }

                    if (type.getType().equalsIgnoreCase("mute")) {
                        if (!s.hasPermission("banSystem.add.customMute")) {
                            s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                            return false;
                        }

                        Random rand = new Random();

                        int random = rand.nextInt(100000);

                        while ((BanService.getInstance().banIDExists(random)) || (MuteService.getInstance().banIDExists(random))) {
                            random = rand.nextInt(100000);
                        }

                        Mute mute = new Mute(random, target.getUniqueId().toString(), reason, endtime, s.getName(), timeStamp, type, permanent, new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date(endtime)), true);
                        MuteService.getInstance().addMute(mute);


                        for(CloudPlayer cloudPlayer : CloudAPI.getInstance().getOnlinePlayers()) {
                            if(NotifyService.getInstance().isPlayerInNotifycationChat(cloudPlayer.getUniqueId())) {
                                cloudPlayer.getPlayerExecutor().sendMessage(cloudPlayer,Main.getInstance().getConfigString("muteNotification", true).replaceAll("%target%", target.getName()).replaceAll("%banner%", s.getName()).replaceAll("%reason%", reason).replaceAll("%time%", time).replaceAll("%banid%",Integer.toString(random)));
                            }
                        }



                        s.sendMessage(Main.getInstance().getConfigString("mute", true));
                        return true;
                    }

                    if (type.getType().equalsIgnoreCase("kick")) {
                        if (!s.hasPermission("banSystem.add.customKick")) {
                            s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                            return false;
                        }
                        if (targetBukkit.isOnline()) {
                            kicked = Bukkit.getPlayer(target.getName());
                            kicked.kickPlayer(Main.getInstance().getConfigString("kicked", false).replaceAll("%reason%", reason));
                            return true;
                        }
                        s.sendMessage(Main.getInstance().getConfigString("noPlayer", true));
                        return false;
                    }
                }

                if (args.length != 3) {
                    s.sendMessage(Main.getInstance().getConfigString("templateSyntax", true));
                    return false;
                }
                if (!ConfigFileManager.configExists(args[1])) {
                    s.sendMessage(Main.getInstance().getConfigString("noTemplate", true));
                    return false;
                }

                de.dytanic.cloudnet.lib.player.OfflinePlayer target = CloudAPI.getInstance().getOfflinePlayer(args[2]);



                if (target == null) {
                    s.sendMessage(Main.getInstance().getConfigString("noPlayer", true));
                    return false;
                }

                OfflinePlayer targetBukkit = Bukkit.getOfflinePlayer(target.getUniqueId());


                if(AdminService.getInstance().isPlayerUnbannable(targetBukkit.getUniqueId())) {
                    s.sendMessage(Main.getInstance().getConfigString("unbannable", true));
                    return false;
                }




                String timeStamp = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date());
                Config config = ConfigFileManager.getConfig(args[1]);
                Type type = config.getType();
                long endtime = System.currentTimeMillis() + Main.getInstance().timeToMilliSeconds(config.getLenght());

                Player kicked;

                if (type.getType().equalsIgnoreCase("ban")) {
                    if (!s.hasPermission("banSystem.add.ban")) {
                        s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                        return false;
                    }
                    if (BanService.getInstance().isPlayerBanned(targetBukkit)) {
                        s.sendMessage(Main.getInstance().getConfigString("alreadyBanned", true));
                        return false;
                    }

                    Random rand = new Random();

                    int random = rand.nextInt(100000);

                    while ((BanService.getInstance().banIDExists(random)) || (MuteService.getInstance().banIDExists(random))) {
                        random = rand.nextInt(100000);
                    }

                    if(BanService.getInstance().getBannedForReason(targetBukkit,config.getReason()) == 1) {
                        endtime = System.currentTimeMillis() + -2 * Main.getInstance().timeToMilliSeconds(config.getLenght());
                    }

                    if(BanService.getInstance().getBannedForReason(targetBukkit,config.getLenght()) == 2) {
                        endtime = System.currentTimeMillis() + 3 * Main.getInstance().timeToMilliSeconds(config.getLenght());
                    }

                    if(BanService.getInstance().getBannedForReason(targetBukkit,config.getLenght()) > 2) {
                        endtime = System.currentTimeMillis() + 3 * Main.getInstance().timeToMilliSeconds(config.getLenght());
                        config.setPermanent(true);
                    }


                    Ban ban = new Ban(random, targetBukkit.getUniqueId(), config.getReason(), endtime, s.getName(), timeStamp, config.getType(), config.getPermanent(), new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date(endtime)), true);

                    BanService.getInstance().addBan(ban);
                    String time = config.getLenght();

                    if (config.getPermanent()) {
                        time = "PERMANENT";
                    }

                    if (CloudAPI.getInstance().getOnlinePlayer(target.getUniqueId()) != null) {
                        CloudAPI.getInstance().getOnlinePlayer(target.getUniqueId()).getPlayerExecutor().kickPlayer(null,"§4§lBanned!");
                    }

                    for(CloudPlayer cloudPlayer : CloudAPI.getInstance().getOnlinePlayers()) {
                        if(NotifyService.getInstance().isPlayerInNotifycationChat(cloudPlayer.getUniqueId())) {
                            cloudPlayer.getPlayerExecutor().sendMessage(cloudPlayer,Main.getInstance().getConfigString("banNotification", true).replaceAll("%target%", targetBukkit.getName()).replaceAll("%banner%", s.getName()).replaceAll("%reason%", config.getReason()).replaceAll("%time%", time).replaceAll("%banid%",Integer.toString(random)));
                        }
                    }



                    s.sendMessage(Main.getInstance().getConfigString("bannedCostume", true));
                    return true;
                }

                if (type.getType().equalsIgnoreCase("mute")) {
                    if (!s.hasPermission("banSystem.add.mute")) {
                        s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                        return false;
                    }
                    if (MuteService.getInstance().isPlayerMuted(targetBukkit)) {
                        s.sendMessage(Main.getInstance().getConfigString("alreadyBanned", true));
                        return false;
                    }

                    Random rand = new Random();
                    int random = rand.nextInt(10000);
                    while (MuteService.getInstance().banIDExists(random)) {
                        random = rand.nextInt(10000);
                    }


                    if(MuteService.getInstance().getBannedForReason(targetBukkit,config.getReason()) == 1) {
                        endtime = System.currentTimeMillis() + -2 * Main.getInstance().timeToMilliSeconds(config.getLenght());
                        s.sendMessage(Main.getInstance().getConfigString("banAutomaticExtended",true));
                    }

                    if(MuteService.getInstance().getBannedForReason(targetBukkit,config.getLenght()) == 2) {
                        endtime = System.currentTimeMillis() + 3 * Main.getInstance().timeToMilliSeconds(config.getLenght());
                        s.sendMessage(Main.getInstance().getConfigString("banAutomaticExtended",true));
                    }

                    if(MuteService.getInstance().getBannedForReason(targetBukkit,config.getLenght()) > 2) {
                        endtime = System.currentTimeMillis() + 3 * Main.getInstance().timeToMilliSeconds(config.getLenght());
                        s.sendMessage(Main.getInstance().getConfigString("banAutomaticExtended",true));
                        config.setPermanent(true);
                    }


                    Mute mute = new Mute(random, targetBukkit.getUniqueId().toString(), config.getReason(), endtime , s.getName(), new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()), type, config.getPermanent(), new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date(endtime)), true);
                    MuteService.getInstance().addMute(mute);
                    if(config.getLenght().contains("p")) config.setLenght("PERMANENT");

                    for(CloudPlayer cloudPlayer : CloudAPI.getInstance().getOnlinePlayers()) {
                        if(NotifyService.getInstance().isPlayerInNotifycationChat(cloudPlayer.getUniqueId())) {
                            cloudPlayer.getPlayerExecutor().sendMessage(cloudPlayer,Main.getInstance().getConfigString("muteNotification", true).replaceAll("%target%", targetBukkit.getName()).replaceAll("%banner%", s.getName()).replaceAll("%reason%", config.getReason()).replaceAll("%time%", config.getLenght()).replaceAll("%banid%",Integer.toString(random)));
                        }
                    }

                    s.sendMessage(Main.getInstance().getConfigString("mute", true));
                    return true;
                }

                if (type.getType().equalsIgnoreCase("kick")) {
                    if (!s.hasPermission("banSystem.add.kick")) {
                        s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                        return false;
                    }
                    if (targetBukkit.isOnline()) {
                        kicked = Bukkit.getPlayer(targetBukkit.getName());
                        kicked.kickPlayer(Main.getInstance().getConfigString("kicked", false).replaceAll("%reason%", config.getReason()));
                        return true;
                    }
                    s.sendMessage(Main.getInstance().getConfigString("noPlayer", true));
                    return false;
                }
            }
            case "REMOVE": {
                if (!s.hasPermission("banSystem.remove")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 3) {
                    s.sendMessage(Main.getInstance().getConfigString("removeSyntax", true));
                    return false;
                }

                de.dytanic.cloudnet.lib.player.OfflinePlayer target = CloudAPI.getInstance().getOfflinePlayer(args[2]);

                if (target == null) {
                    s.sendMessage(Main.getInstance().getConfigString("noPlayer", true));
                    return false;
                }

                OfflinePlayer targetBukkit = Bukkit.getOfflinePlayer(target.getUniqueId());

                int banID;
                try {
                    banID = Integer.parseInt(args[1]);
                } catch (Exception e) {
                    s.sendMessage(Main.getInstance().getConfigString("removeSyntax", true));
                    return false;
                }

                if(BanService.getInstance().banIDExists(banID)) {
                    if(BanService.getInstance().isPlayerBanned(targetBukkit)) {
                        BanService.getInstance().removeBan(banID);
                        s.sendMessage(Main.getInstance().getConfigString("banRemoved", true));
                        return true;
                    }
                    s.sendMessage(Main.getInstance().getConfigString("noBan",true));
                    return false;
                }
                if(MuteService.getInstance().banIDExists(banID)) {
                    if(MuteService.getInstance().isPlayerMuted(targetBukkit)) {
                        MuteService.getInstance().removeMute(banID);
                        s.sendMessage(Main.getInstance().getConfigString("banRemoved", true));
                        return true;
                    }
                    s.sendMessage(Main.getInstance().getConfigString("noBan",true));
                    return false;
                }

                s.sendMessage(Main.getInstance().getConfigString("noBanID", true));
                return false;

            }
            case "RELOAD": {
                if (!s.hasPermission("banSystem.reload")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                ConfigFileManager.reloadConfig();
                Main.getInstance().reloadConfig();
                s.sendMessage(Main.getInstance().getConfigString("reload", true));
                return true;
            }
            case "CHECK": {
                if (!s.hasPermission("banSystem.check")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 2) {
                    s.sendMessage(Main.getInstance().getConfigString("checkSyntax", true));
                    return false;
                }

                de.dytanic.cloudnet.lib.player.OfflinePlayer target = CloudAPI.getInstance().getOfflinePlayer(args[1]);

                if (target == null) {
                    s.sendMessage(Main.getInstance().getConfigString("noPlayer", true));
                    return false;
                }

                OfflinePlayer op = Bukkit.getOfflinePlayer(target.getUniqueId());


                s.sendMessage(Main.getInstance().getConfigString("infos", true).replaceAll("%target%", op.getName()));
                s.sendMessage("");
                s.sendMessage("§4§l>> §7Bans:");
                s.sendMessage("");
                if (!BanService.getInstance().hadBeenBanned(op)) {
                    s.sendMessage("§7The player doesn't have any bans in his history yet.");
                } else {
                    for (Ban ban : BanService.getInstance().getAllBans(op)) {
                        s.sendMessage("§c - §7" + ban.getBanID() + "§c | §7" + ban.getTimeStamp() + "§c | §7active :" + ban.isActive());
                    }
                }
                s.sendMessage("");
                s.sendMessage("§4§l>> §7Mute:");
                s.sendMessage("");
                if (!MuteService.getInstance().hadBeenMuted(op)) {
                    s.sendMessage("§7The player doesn't have any mutes in his history yet.");
                } else {
                    for (Mute mute : MuteService.getInstance().getAllMutes(op)) {
                        s.sendMessage("§c - §7" + mute.getBanID() + "§c | §7" + mute.getTimeStamp() + "§c | §7active : " + mute.isActive());
                    }
                }
                s.sendMessage("");
                return true;
            }
            case "IDINFO": {
                if (!s.hasPermission("banSystem.idInfo")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 2) {
                    s.sendMessage(Main.getInstance().getConfigString("idinfoSyntax", true));
                    return false;
                }
                int banID;
                try{
                    banID = Integer.parseInt(args[1]);
                }catch (Exception e) {
                    s.sendMessage(Main.getInstance().getConfigString("idinfoSyntax", true));
                    return false;
                }

                Mute m;
                Ban b;

                if(MuteService.getInstance().banIDExists(banID)) {
                    m = MuteService.getInstance().getMute(banID);
                    s.sendMessage(Main.getInstance().getConfigString("Prefix",false));
                    s.sendMessage("");
                    s.sendMessage("§7BanID: §c" + banID);
                    s.sendMessage("§7Reason: §c" + m.getReason());
                    s.sendMessage("§7Type: §c" + m.getType());
                    s.sendMessage("§7Created: §c" + m.getTimeStamp());
                    s.sendMessage("§7Unbanned: §c" + m.getUnbanned());
                    s.sendMessage("§7Active: §c" + m.isActive());
                    s.sendMessage("§7Permanent: §c" + m.isPermanent());
                    s.sendMessage("§7Banner: §c" + m.getBanner());
                    s.sendMessage("");
                    return true;
                }else if(BanService.getInstance().banIDExists(banID)) {
                    b = BanService.getInstance().getBan(banID);
                    s.sendMessage(Main.getInstance().getConfigString("Prefix",false));
                    s.sendMessage("");
                    s.sendMessage("§7BanID: §c" + banID);
                    s.sendMessage("§7Reason: §c" + b.getReason());
                    s.sendMessage("§7Type: §c" + b.getType());
                    s.sendMessage("§7Created: §c" + b.getTimeStamp());
                    s.sendMessage("§7Unbanned: §c" + b.getUnbannedDate());
                    s.sendMessage("§7Active: §c" + b.isActive());
                    s.sendMessage("§7Permanent: §c" + b.isPermanent());
                    s.sendMessage("§7Banner: §c" + b.getBanner());
                    s.sendMessage("");
                    return true;
                }else{
                    s.sendMessage(Main.getInstance().getConfigString("noBanID",true));
                    return false;
                }





            }
            case "CLEAR": {
                if (!s.hasPermission("banSystem.clear")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 3) {
                    s.sendMessage(Main.getInstance().getConfigString("clearSyntax", true));
                    return false;
                }

                if(!isBanType(args[1])) {
                    s.sendMessage(Main.getInstance().getConfigString("noType", true));
                    return false;
                }
                Type type = new Type(Type.BanType.valueOf(args[1].toUpperCase()));

                de.dytanic.cloudnet.lib.player.OfflinePlayer target = CloudAPI.getInstance().getOfflinePlayer(args[2]);

                if (target == null) {
                    s.sendMessage(Main.getInstance().getConfigString("noPlayer", true));
                    return false;
                }

                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(target.getUniqueId());

                if(target == null) {
                    s.sendMessage(Main.getInstance().getConfigString("noPlayer",true));
                    return false;
                }

                if(type.getType().equalsIgnoreCase("ban")) {
                    if(!BanService.getInstance().hadBeenBanned(offlinePlayer)) {
                        s.sendMessage(Main.getInstance().getConfigString("noBan", true));
                        return false;
                    }
                    BanService.getInstance().removeAllBans(offlinePlayer);
                    s.sendMessage(Main.getInstance().getConfigString("allBanRemoved",true));
                    return true;

                }else if(type.getType().equalsIgnoreCase("mute")) {
                    if(!MuteService.getInstance().hadBeenMuted(offlinePlayer)) {
                        s.sendMessage(Main.getInstance().getConfigString("noBan", true));
                        return false;
                    }
                    MuteService.getInstance().removeAllMutes(offlinePlayer);
                    s.sendMessage(Main.getInstance().getConfigString("allBanRemoved",true));
                    return true;
                }
                s.sendMessage(Main.getInstance().getConfigString("noType", true));
                return false;
            }
            case "NOTIFY": {

                if(!(s instanceof Player)) {
                    s.sendMessage("You must be a player for this specific sub-command!");
                    return false;
                }

                Player p = (Player) s;

                if (!s.hasPermission("banSystem.notify")) {
                    s.sendMessage(Main.getInstance().getConfigString("noPermissions", true));
                    return false;
                }
                if (args.length != 1) {
                    s.sendMessage(Main.getInstance().getConfigString("notifySyntax", true));
                    return false;
                }
                if(NotifyService.getInstance().isPlayerInNotifycationChat(p.getUniqueId())) {
                    NotifyService.getInstance().removePlayer(new NotifyID(p.getUniqueId()));
                    s.sendMessage(Main.getInstance().getConfigString("LeftNotify", true));
                    return true;
                }
                NotifyService.getInstance().addPlayer(new NotifyID(p.getUniqueId()));
                s.sendMessage(Main.getInstance().getConfigString("JoinedNotify", true));
                return true;
            }
        }
        s.sendMessage(Main.getInstance().getConfigString("Syntax", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax1", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax2", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax3", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax4", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax5", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax6", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax7", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax8", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax9", true));
        s.sendMessage(Main.getInstance().getConfigString("Syntax10", true));
        return false;
    }

    private boolean isBanType(String type)
    {
        for (Type.BanType c : Type.BanType.values()) {
            if (type.equalsIgnoreCase(c.toString())) {
                return true;
            }
        }
        return false;
    }
}
