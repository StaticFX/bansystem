/*
 * Decompiled with CFR <Could not determine version>.
 */
package de.staticfx.bansystem.objects;

public class Type {
    BanType type;

    public Type(BanType type) {
        this.type = type;
    }

    public String getType() {
        return this.type.toString();
    }

    public static enum BanType {
        BAN,
        MUTE,
        KICK;
        
    }

}

