/*
 * Decompiled with CFR <Could not determine version>.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 */
package de.staticfx.bansystem.events;

import de.staticfx.bansystem.Main;
import de.staticfx.bansystem.db.MuteService;
import de.staticfx.bansystem.objects.Mute;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class OnChatMessage
implements Listener {
    @EventHandler
    public void onMessage(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();

        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(p.getUniqueId());

        if(MuteService.getInstance().isPlayerMuted(offlinePlayer)) {
            for(Mute m : MuteService.getInstance().getAllMutes(offlinePlayer)) {
                if(m.isActive()) {
                    if(m.getEndTime() > System.currentTimeMillis() || m.isPermanent()) {
                        e.setCancelled(true);

                        String date;
                        if(m.isPermanent()) {
                            date = "PERMANENT";
                        }else{
                            date = m.getUnbanned();
                        }
                        p.sendMessage(Main.getInstance().getConfigString("muted",true).replaceAll("%reason%",m.getReason()).replaceAll("%time%",date).replaceAll("%banid%",Integer.toString(m.getBanID())));
                    }else{
                        MuteService.getInstance().removeMute(m.getBanID());
                    }
                }
            }


        }

    }
}

