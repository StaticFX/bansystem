/*
 * Decompiled with CFR <Could not determine version>.
 */
package de.staticfx.bansystem.objects;

import de.staticfx.bansystem.objects.Type;

public class Config {
    String name;
    String date;
    String reason;
    Type type;
    Boolean permanent;

    public Config(String name, String time, String reason, Type type, boolean permanent) {
        this.permanent = permanent;
        this.name = name;
        this.date = time;
        this.reason = reason;
        this.type = type;
    }


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLenght() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setLenght(String lenght) {
        lenght = date;
    }

    public Boolean getPermanent() {
        return permanent;
    }

    public void setPermanent(Boolean permanent) {
        this.permanent = permanent;
    }
}

