/*
 * Decompiled with CFR <Could not determine version>.
 */
package de.staticfx.bansystem.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import de.staticfx.bansystem.objects.Type;

import java.util.UUID;

@DatabaseTable(tableName="bans")
public class Ban {
    @DatabaseField(id=true, canBeNull=false)
    private int BanID;
    @DatabaseField()
    private UUID uuid;
    @DatabaseField
    private String reason;
    @DatabaseField
    private Long endTime;
    @DatabaseField
    private String banner;
    @DatabaseField
    private String timeStamp;
    @DatabaseField
    private String type;
    @DatabaseField
    private boolean permanent;
    @DatabaseField
    private String unbannedDate;
    @DatabaseField
    private boolean active;

    public Ban() {
    }

    public Ban(int banID, UUID uuid, String reason, Long endTime, String banner, String timeStamp, Type type, boolean permanent, String unbannedDate, boolean active) {
        this.uuid = uuid;
        this.reason = reason;
        this.endTime = endTime;
        this.banner = banner;
        this.timeStamp = timeStamp;
        this.type = type.getType();
        this.permanent = permanent;
        this.unbannedDate = unbannedDate;
        this.BanID = banID;
        this.active = active;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public boolean isPermanent() {
        return this.permanent;
    }

    public void setPermanent(boolean permanent) {
        this.permanent = permanent;
    }

    public String getUnbannedDate() {
        return this.unbannedDate;
    }

    public void setUnbannedDate(String unbannedDate) {
        this.unbannedDate = unbannedDate;
    }

    public String getBanner() {
        return this.banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public int getBanID() {
        return this.BanID;
    }

    public void setBanID(int banID) {
        this.BanID = banID;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean activ) {
        this.active = activ;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

