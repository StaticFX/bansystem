package de.staticfx.bansystem.db;

import com.j256.ormlite.db.MysqlDatabaseType;

public class DataType extends MysqlDatabaseType {

    @Override
    protected String getDriverClassName() {
        return "org.mariadb.jdbc.Driver";
    }

    @Override
    public void loadDriver() {
    }
}
