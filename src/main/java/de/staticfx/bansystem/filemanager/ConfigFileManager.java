/*
 * Decompiled with CFR <Could not determine version>.
 * 
 * Could not load the following classes:
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.YamlConfiguration
 */
package de.staticfx.bansystem.filemanager;

import de.staticfx.bansystem.Main;
import de.staticfx.bansystem.objects.Config;
import de.staticfx.bansystem.objects.Type;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigFileManager {
    private static File file = new File(Main.getInstance().getDataFolder().getAbsolutePath(), "templates.yml");
    private static FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration((File)file);

    public static void loadFile() {
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            fileConfiguration.load(file);
            return;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFile() {
        try {
            fileConfiguration.save(file);
            return;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addConfig(Config config) {
        fileConfiguration.set(config.getName() + ".name", (Object)config.getName());
        fileConfiguration.set(config.getName() + ".type", (Object)config.getType().getType());
        fileConfiguration.set(config.getName() + ".length", (Object)config.getLenght());
        fileConfiguration.set(config.getName() + ".reason", (Object)config.getReason());
        fileConfiguration.set(config.getName() + ".permanent", (Object)config.getPermanent());
        ConfigFileManager.saveFile();
    }

    public static void removeConfig(String config) {
        fileConfiguration.set(config + ".name", null);
        fileConfiguration.set(config + ".type", null);
        fileConfiguration.set(config + ".length", null);
        fileConfiguration.set(config + ".reason", null);
        fileConfiguration.set(config + ".permanent", null);
        fileConfiguration.set(config, null);
        ConfigFileManager.saveFile();
    }

    public static boolean configExists(String name) {
        return fileConfiguration.contains(name);
    }

    public static Config getConfig(String name) {
        String configName = fileConfiguration.getString(name + ".name");
        String type = fileConfiguration.getString(name + ".type");
        String lenght = fileConfiguration.getString(name + ".length");
        String reason = fileConfiguration.getString(name + ".reason");
        boolean permanent = fileConfiguration.getBoolean(name + ".permanent");
        return new Config(configName, lenght, reason, new Type(Type.BanType.valueOf(type)), permanent);
    }

    public static void reloadConfig() {
        ConfigFileManager.loadFile();
    }

    public static ArrayList<String> getConfigs() {
        return new ArrayList<>(fileConfiguration.getKeys(false));
    }
}

