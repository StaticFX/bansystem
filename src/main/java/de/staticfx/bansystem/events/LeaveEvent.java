package de.staticfx.bansystem.events;

import de.staticfx.bansystem.Main;
import de.staticfx.bansystem.db.BanService;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveEvent implements Listener {

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {

        Player p = e.getPlayer();

        e.setQuitMessage("");

    }

}
