package de.staticfx.bansystem.objects;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;

@DatabaseTable(tableName = "notify")
public class NotifyID {

    @DatabaseField(id=true, canBeNull=false)
    private UUID uuid;

    public NotifyID() {
    }

    public NotifyID(UUID uuid) {
        this.uuid = uuid;
    }


    public UUID getUuid() {
        return uuid;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

}
