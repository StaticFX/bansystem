package de.staticfx.bansystem.objects;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.bukkit.entity.Player;

import java.util.UUID;

@DatabaseTable(tableName="unbannable")
public class PlayerID {

    @DatabaseField(id=true, canBeNull=false)
    private UUID uuid;

    public PlayerID() {
    }

    public PlayerID(UUID uuid) {
        this.uuid = uuid;
    }
    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
