/*
 * Decompiled with CFR <Could not determine version>.
 * 
 * Could not load the following classes:
 *  org.bukkit.OfflinePlayer
 */
package de.staticfx.bansystem.db;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import de.staticfx.bansystem.objects.Mute;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.bukkit.OfflinePlayer;

public class MuteService {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private static final MuteService INSTANCE = new MuteService();
    private Dao<Mute, Integer> muteDao = OrmliteService.getInstance().createDao(Mute.class);

    private MuteService() {
    }

    public static MuteService getInstance() {
        return INSTANCE;
    }

    public void addMute(Mute mute) {
        try {
            this.muteDao.create(mute);
            return;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Mute getMute(int banID) {
        try {
            return this.muteDao.queryForId(banID);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean banIDExists(int banID) {
        try {
            return this.muteDao.idExists(banID);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void removeMute(int banID) {
        Mute mute = this.getMute(banID);
        mute.setActive(false);
        try {
            this.muteDao.update(mute);
            return;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean hadBeenMuted(OfflinePlayer offlinePlayer) {
        return !getAllMutes(offlinePlayer).isEmpty();
    }


    public List<Mute> getAllMutes(OfflinePlayer offlinePlayer) {
        try {
            QueryBuilder<Mute, Integer> query = this.muteDao.queryBuilder();
            Where queryWhere = query.where();
            queryWhere.eq("uuid", offlinePlayer.getUniqueId());
            PreparedQuery<Mute> mutes = query.prepare();
            return this.muteDao.query(mutes);
        }
        catch (Exception e) {
            return null;
        }
    }

    public boolean isPlayerMuted(OfflinePlayer offlinePlayer) {
        return getAllMutes(offlinePlayer).stream().anyMatch(Mute::isActive);
    }

    public void removeAllMutes(OfflinePlayer offlinePlayer) {
        List<Mute> ban = getAllMutes(offlinePlayer);
        for(Mute b : ban) {
            try{
                muteDao.delete(b);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getBannedForReason(OfflinePlayer offlinePlayer, String reason) {
        List<Mute> bans = getAllMutes(offlinePlayer);
        int result = 0;

        for(Mute b : bans) {
            if(b.getReason().equalsIgnoreCase(reason));
            result++;
        }
        return result;
    }


}

