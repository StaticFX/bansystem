package de.staticfx.bansystem.db;

import com.j256.ormlite.dao.Dao;
import de.staticfx.bansystem.objects.NotifyID;
import de.staticfx.bansystem.objects.PlayerID;

import java.sql.SQLException;
import java.util.UUID;

public class NotifyService {

    private static final NotifyService INSTANCE = new NotifyService();
    private Dao<NotifyID, UUID> notifyDao = OrmliteService.getInstance().createDao(NotifyID.class);


    private NotifyService() {
    }

    public static NotifyService getInstance() {
        return INSTANCE;
    }

    public void addPlayer(NotifyID notifyID) {
        try {
            notifyDao.create(notifyID);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removePlayer(NotifyID notifyID) {
        try {
            notifyDao.delete(notifyID);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isPlayerInNotifycationChat(UUID uuid) {
        try{
            return notifyDao.idExists(uuid);
        }catch (Exception e) {
            return false;
        }
    }


}
