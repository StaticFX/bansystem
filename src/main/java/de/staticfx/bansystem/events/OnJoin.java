/*
 * Decompiled with CFR <Could not determine version>.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.player.PlayerJoinEvent
 */
package de.staticfx.bansystem.events;

import de.staticfx.bansystem.Main;
import de.staticfx.bansystem.db.AdminService;
import de.staticfx.bansystem.db.BanService;
import de.staticfx.bansystem.objects.Ban;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import de.staticfx.bansystem.objects.PlayerID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class OnJoin
implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();


        PlayerID playerID = new PlayerID(p.getUniqueId());

        if(p.hasPermission("banSystem.deny")) {
            if(!AdminService.getInstance().isPlayerUnbannable(p.getUniqueId())) {
                AdminService.getInstance().addPlayer(playerID);
            }
        }else{
            if(AdminService.getInstance().isPlayerUnbannable(p.getUniqueId())) {
                AdminService.getInstance().removePlayer(playerID);
            }
        }


        OfflinePlayer op = Bukkit.getOfflinePlayer(p.getUniqueId());

        e.setJoinMessage("");

        if(BanService.getInstance().isPlayerBanned(op)) {
            for(Ban ban : BanService.getInstance().getAllBans(op)) {
                if(ban.getEndTime() > System.currentTimeMillis()) {
                    p.kickPlayer(Main.getInstance().getConfigString("BanMessage",false));
                    return;
                }else{
                    BanService.getInstance().removeBan(ban.getBanID());
                }
            }
        }
    }
}

